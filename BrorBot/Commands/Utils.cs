﻿using Discord.Commands;
using System.Threading.Tasks;

namespace BrorBot.Commands
{
    public class Utils : ModuleBase
    {
        [Command("ping")]
        public async Task Ping()
        {
            await ReplyAsync("Pong! :ping_pong:");
        }
    }
}
