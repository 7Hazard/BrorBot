﻿using Discord;
using Discord.Audio;
using Discord.Commands;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace BrorBot.Commands
{
    public class Audio : ModuleBase
    {
        public static bool Playing = false;

        [Command("clips")]
        public async Task GetAudioClips()
        {
            StringBuilder builder = new StringBuilder();
            string[] files = Directory.GetFiles(@"Audio/", "*.opus");
            foreach (string file in files)
            {
                builder.Append(Path.GetFileNameWithoutExtension(file));
                builder.Append(", ");
            }

            var dm = await Context.User.CreateDMChannelAsync();
            await dm.SendMessageAsync("``"+builder.ToString()+"``");
        }

        [Command("play", RunMode = RunMode.Async)]
        public async Task BroadcastAudio(string audio, IVoiceChannel channel = null)
        {
            channel = channel ?? (Context.User as IGuildUser)?.VoiceChannel;
            var dm = await Context.User.CreateDMChannelAsync();
            if (channel == null)
            {
                await dm.SendMessageAsync("You have to be in a channel to .play or put in a channel name.");
                return;
            }
            else if (channel.Position == 0)
            {
                await dm.SendMessageAsync("Cannot .play in the Lobby.");
                return;
            }

            string path = @"Audio/" + audio + ".opus";
            if (!File.Exists(path))
            {
                await dm.SendMessageAsync("The audio clip does not exist! Type .clips to list all of the clips.");
                return;
            }

            if (!Playing)
            {
                Playing = true;
                var audioClient = await channel.ConnectAsync();
                var ffmpeg = CreateStream(path); ;
                var output = ffmpeg.StandardOutput.BaseStream;
                var discord = audioClient.CreatePCMStream(AudioApplication.Mixed, 1920);
                await output.CopyToAsync(discord);
                await discord.FlushAsync();
                await audioClient.StopAsync();
                Playing = false;
            } else
            {
                await dm.SendMessageAsync("An audio clip is already being played. Try again after.");
                return;
            }
        }

        private Process CreateStream(string path)
        {
            var ffmpeg = new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $"-i {path} -ac 2 -f s16le -ar 48000 pipe:1",
                UseShellExecute = false,
                RedirectStandardOutput = true,
            };
            return Process.Start(ffmpeg);
        }
    }
}
