﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace BrorBot
{
    public class Program
    {
        public static DiscordSocketClient client = new DiscordSocketClient();
        public static CommandService commands = new CommandService();
        public static DependencyMap dependencyMap = new DependencyMap();
        static MessageHandler msgHandler = new MessageHandler();

        public static void Main(string[] args)
            => new Program().Start().GetAwaiter().GetResult();

        public async Task Start()
        {
            client.Log += Log;
            client.MessageReceived += msgHandler.MessageReceived;

            await client.LoginAsync(TokenType.Bot, "MTgzNzM1NTIxMDcwMTUzNzI4.C9-OyA.v1PnzlOxdVQezkP1b-iUyW8Os00");
            await client.StartAsync();
            await commands.AddModulesAsync(Assembly.GetEntryAssembly());

            // Block this task until the program is closed.
            await Task.Delay(-1);
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}