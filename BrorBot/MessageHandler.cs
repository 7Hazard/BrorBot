﻿using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Threading.Tasks;
using static BrorBot.Program;

namespace BrorBot
{
    public class MessageHandler
    {
        public async Task MessageReceived(SocketMessage messageParam)
        {
            var message = messageParam as SocketUserMessage;
            if (message == null) return;
            int argPos = 0;
            if (message.Channel is SocketDMChannel)
            {
                Console.WriteLine(message.Author.Username + " DM'ed: \n\t" + message);
                if (!(message.HasCharPrefix('.', ref argPos) || message.HasMentionPrefix(client.CurrentUser, ref argPos))) return;
            } else
            {
                Console.WriteLine(message.Author.Username + " said in "+message.Channel.Name.ToUpper()+": \n\t" + message);
                if (!(message.HasCharPrefix('.', ref argPos) || message.HasMentionPrefix(client.CurrentUser, ref argPos))) return;
                await message.DeleteAsync();
            }

            var context = new CommandContext(client, message);
            var result = await commands.ExecuteAsync(context, argPos, dependencyMap);
            if (!result.IsSuccess)
            {
                var dm = await context.User.CreateDMChannelAsync();
                await dm.SendMessageAsync(result.ErrorReason);
            }
        }
    }
}
